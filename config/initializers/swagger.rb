Swagger::Docs::Config.register_apis({
	"1.0" => {
		:api_extension_type => :json,
		:api_file_path => "public/api/v1/",
		:base_path => "htpps://simbirsoft-test-test.heroku.com",
		:clean_directory => true,
#		:parent_controller => Api::V1::SearchController,
		:attributes => {
			:info => {
				"title" => "Stack Simbir Search App",
				"description" => "This is a test app for SSoft",
				"contact" => "a.v.kalionov@gmail.com"
			}
		}
	}
})
