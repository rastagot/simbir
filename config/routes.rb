Rails.application.routes.draw do
#  get 'welcome/index'
  root :to => 'search#index'
  get 'search/index'
# post 'search/search'
#  get 'search/search'

  #api routes. Don't know if this is ok ^_^

  get '/search/search.json', to: 'search#search_to_backend'
  post 'search/search', to: 'search#search_to_frontend'
  get 'search/search', to: 'search#search_to_frontend'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
