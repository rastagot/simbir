class Question < ApplicationRecord

  def self.search ( params )
	conn = Faraday.new(:url => 'https://api.stackexchange.com' )
        resp = conn.get do |req|
                req.url '/2.2/search/advanced'

	        req.options.timeout = 5		
		req.options.open_timeout = 5
                req.params['order'] = "desc"
                req.params['sort'] = "activity"
                req.params['site'] = "stackoverflow"
		if (params.has_key?(:searchstring))
		  req.params['q'] = params[:searchstring]
		end
		if (params.has_key?(:page))
                  req.params['p'] = params['page']
		end
        end
	resp.body
  end

  def self.parse_to_frontend ( body )
	@results = JSON.parse body
	@results = @results['items']
	@results.map { |result|
		result['creation_date'] = DateTime.strptime("#{result['creation_date']}", '%s')
	}
	
	@results
  end

  def self.parse_to_backend ( body)
	  @results_temp = JSON.parse body
	  @results_temp = @results_temp['items']

	  @results = Array.new()

	  @results_temp.each do |result| 
		res = { :title => result['title'], :creation_date => result['creation_date'], :link => result['link']}
		@results.push(res)
          end

	  @results
  end

  def self.search_to_frontend ( params )
	  @res = self.search ( params)
	  @result = self.parse_to_frontend (@res)  

	  @result
  end	  

  def self.search_to_backend ( params )
	  @res = self.search ( params )
	  @result = self.parse_to_backend (@res)

	  @result
  end

end
