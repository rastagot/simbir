class SearchController < ApplicationController
  include Response

   swagger_controller :search, "SearchController"

   swagger_api :search do
	   summary "Searches stack for string"
	   param :form, :searchstring, :string, :required, "Search String"
	   param :page, :page, :integer, :optional, "Page number"
	   response :ok, "Success"
   end

   def search
   end

   def index
     @question = Question.new
   end

   def search_to_frontend
     @results = Question.search_to_frontend(params)
	
#     json_response(@results)
   end

   def search_to_backend
     @results = Question.search_to_backend(params)
     json_response(@results)
   end
end
